DESTDIR ?=

DLM2SLOG2_VERSION = 0.0.2

prefix = /usr
manifestfile = MANIFEST.MF
gen_manifestfile = GEN_MANIFEST.MF
bindir = $(prefix)/bin
datadir = $(prefix)/share
javadir = $(datadir)/java

mfclasspath = $(javadir)/tracecmd.jar $(javadir)/traceTOslog2.jar
classpath = $(javadir)/tracecmd.jar:$(javadir)/traceTOslog2.jar

INSTALL = install

all: dlm2slog2-$(DLM2SLOG2_VERSION).jar

build:
	mkdir -p build
	javac -Xlint:deprecation -d build/ -classpath $(classpath) dlm/linux/*.java dlm/gui/*.java dlm/slog2/output/*.java

dlm2slog2-$(DLM2SLOG2_VERSION).jar: build
	sed '1s|^.*$$|Class-Path: $(mfclasspath)|' $(manifestfile) > $(gen_manifestfile)
	cd build/ && jar cfm ../dlm2slog2-$(DLM2SLOG2_VERSION).jar ../$(gen_manifestfile) dlm
	ln -sf dlm2slog2-$(DLM2SLOG2_VERSION).jar dlm2slog2.jar

jar: dlm2slog2-$(DLM2SLOG2_VERSION).jar force

install: dlm2slog2-$(DLM2SLOG2_VERSION).jar force
	mkdir -p tmp
	cp bin/dlm2slog2 tmp/
	sed -i 's|JARDIR=.*|JARDIR=$(javadir)|' tmp/dlm2slog2
	mkdir -p $(DESTDIR)/$(bindir)
	$(INSTALL) -m 755 tmp/dlm2slog2 '$(DESTDIR)/$(bindir)'
	mkdir -p $(DESTDIR)/$(javadir)
	$(INSTALL) -m 644 dlm2slog2-$(DLM2SLOG2_VERSION).jar '$(DESTDIR)/$(javadir)'
	ln -sf dlm2slog2-$(DLM2SLOG2_VERSION).jar $(DESTDIR)/$(javadir)/dlm2slog2.jar

clean:
	rm -rf *.jar $(gen_manifestfile)
	rm -rf build/ tmp/

force:
.PHONY: clean force jar
