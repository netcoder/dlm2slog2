# dlm2slog2

dlm2slog2

## Getting started

Please see wiki page: [How to use](https://gitlab.com/netcoder/dlm2slog2/-/wikis)

## Dependencies

trace-cmd-java: https://gitlab.com/netcoder/trace-cmd-java
slog2sdk: https://gitlab.com/netcoder/slog2sdk
