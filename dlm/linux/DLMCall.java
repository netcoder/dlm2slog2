// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import java.util.*;
import tracecmd.*;

public class DLMCall
{
	private Integer ls_id;
	private Integer lkb_id;
	private Integer pid;

	public DLMCall(TraceCmdEvent e)
	{
		this.ls_id = e.getNumField("ls_id").intValue();
		this.lkb_id = e.getNumField("lkb_id").intValue();
		this.pid = e.getPid();
	}

	@Override
	public boolean equals(Object o)
	{
		DLMCall call;

		if (o instanceof DLMCall) {
			call = (DLMCall)o;

			return this.ls_id.equals(call.ls_id) &&
			       this.lkb_id.equals(call.lkb_id) &&
			       this.pid.equals(call.pid);
		}

		return false;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.ls_id, this.lkb_id, this.pid);
	}
}
