// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public class DLMArrow extends DLMObject
{
	DLMMessageSend from;
	DLMMessageRecv to;

	public DLMArrow(DLMMessageSend from, DLMMessageRecv to)
	{
		super(to.getTs());

		this.from = from;
		this.to = to;

		switch (from.getType()) {
		case 1:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_REQUEST);
			break;
		case 2:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_CONVERT);
			break;
		case 3:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_UNLOCK);
			break;
		case 4:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_CANCEL);
			break;
		case 5:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_REQUEST_REPLY);
			break;
		case 6:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_CONVERT_REPLY);
			break;
		case 7:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_UNLOCK_REPLY);
			break;
		case 8:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_CANCEL_REPLY);
			break;
		case 9:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_GRANT);
			break;
		case 10:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_BAST);
			break;
		case 11:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_LOOKUP);
			break;
		case 12:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_REMOVE);
			break;
		case 13:
			this.c = DLMCategory.categories.get(DLMCategory.Type.MSG_LOOKUP_REPLY);
			break;
		default:
			break;
		}
	}

	@Override
	public Primitive getPrimitive()
	{
		Primitive p;

		/* TODO Category should be set, should be solved in different way */
		if (this.c == null)
			System.out.println("BUG");

		p = new Primitive(this.c, 2);
		p.setVertex(0, new Coord(DLMHelpers.getSecs(from.getTs()), from.getLineID()));
		p.setVertex(1, new Coord(DLMHelpers.getSecs(to.getTs()), to.getLineID()));
		this.c.setInfoKeys(from.keys());
		p.setInfoBuffer(from.values());

		return p;
	}
}
