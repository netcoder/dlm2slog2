// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import java.nio.*;
import tracecmd.*;

public class DLMEventBast extends DLMEvent
{
	private Integer mode;

        public DLMEventBast(TraceCmdEvent e, Integer nodeid)
        {
		super(e, nodeid);

		this.mode = e.getNumField("mode").intValue();
		this.c = DLMCategory.categories.get(DLMCategory.Type.BAST);
        }

	@Override
	public String keys()
	{
		return super.keys() + ", requesting_mode=%s";
	}

	@Override
        public byte[] values()
        {
		ByteBuffer bb;
		byte[] sbytes;

		String modestr = DLMHelpers.getModeString(this.mode);
		sbytes = super.values();
		bb = ByteBuffer.allocate(sbytes.length + 2 + modestr.getBytes().length);
		bb.put(sbytes);

		bb.putShort((short)modestr.getBytes().length);
		bb.put(modestr.getBytes());

		return bb.array();
        }
}
