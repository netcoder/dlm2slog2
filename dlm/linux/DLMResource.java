// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import java.util.*;
import tracecmd.*;

public class DLMResource implements Comparable<DLMResource>
{
	private Integer ls_id;
	private String res_name;
	private Integer num_events;

	public DLMResource(TraceCmdEvent e)
	{
		this.ls_id = e.getNumField("ls_id").intValue();
		this.res_name = DLMHelpers.getDLMResName(e);
		this.num_events = 0;
	}

	public DLMResource(Integer ls_id, String res_name)
	{
		this.res_name = res_name;
		this.ls_id = ls_id;
		this.num_events = 0;
	}

	public Integer getLsId()
	{
		return this.ls_id;
	}

	public void incNumEvents()
	{
		this.num_events += 1;
	}

	public String getResName()
	{
		return this.res_name;
	}

	public Integer getNumEvents()
	{
		return this.num_events;
	}

	@Override
	public int compareTo(DLMResource o)
	{
		return o.num_events.compareTo(this.getNumEvents());
	}

	@Override
	public String toString()
	{
		return String.format("0x08%x, %s", this.ls_id, this.res_name);
	}

	@Override
	public boolean equals(Object o)
	{
		DLMResource res;

		if (o instanceof DLMResource) {
			res = (DLMResource)o;

			return this.ls_id.equals(res.getLsId()) &&
			       this.res_name.equals(res.getResName());
		}

		return false;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.ls_id, this.res_name);
	}
}
