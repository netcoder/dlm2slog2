// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public abstract class DLMMessage
{
	private Integer line_id;
	private DLMResource res;
	private BigInteger ts;

	private Integer h_seq;
	private Integer nodeid;
	private Integer h_version;
	private Integer h_lockspace;
	private Integer h_nodeid;
	private Integer h_length;
	private Byte h_cmd;
	private Integer m_type;
	private Integer m_nodeid;
	private Integer m_pid;
	private Integer m_lkid;
	private Integer m_remid;
	private Integer m_parent_lkid;
	private Integer m_parent_remid;
	private Integer m_exflags;
	private Integer m_sbflags;
	private Integer m_flags;
	private Integer m_lvbseq;
	private Integer m_hash;
	private Integer m_status;
	private Integer m_grmode;
	private Integer m_rqmode;
	private Integer m_bastmode;
	private Integer m_asts;
	private Integer m_result;
	private Byte[] m_extra;

	public DLMMessage(DLMTraceCmdEvent e,
			  DLMResource res)
	{
		this.res = res;
		this.line_id = Objects.hash(res.hashCode(), e.getNodeId());
		this.ts = e.getTs();

		this.nodeid = e.getNodeId();
		this.h_seq = e.getNumField("h_seq").intValue();
		this.h_version = e.getNumField("h_version").intValue();
		this.h_lockspace = e.getNumField("h_lockspace").intValue();
		this.h_nodeid = e.getNumField("h_nodeid").intValue();
		this.h_length = e.getNumField("h_length").intValue();
		this.h_cmd = e.getNumField("h_cmd").byteValue();
		this.m_type = e.getNumField("m_type").intValue();
		this.m_nodeid = e.getNumField("m_nodeid").intValue();
		this.m_pid = e.getNumField("m_pid").intValue();
		this.m_lkid = e.getNumField("m_lkid").intValue();
		this.m_remid = e.getNumField("m_remid").intValue();
		this.m_parent_lkid = e.getNumField("m_parent_lkid").intValue();
		this.m_parent_remid = e.getNumField("m_parent_remid").intValue();
		this.m_exflags = e.getNumField("m_exflags").intValue();
		this.m_sbflags = e.getNumField("m_sbflags").intValue();
		this.m_flags = e.getNumField("m_flags").intValue();
		this.m_lvbseq = e.getNumField("m_lvbseq").intValue();
		this.m_hash = e.getNumField("m_hash").intValue();
		this.m_status = e.getNumField("m_status").intValue();
		this.m_grmode = e.getNumField("m_grmode").intValue();
		this.m_rqmode = e.getNumField("m_rqmode").intValue();
		this.m_bastmode = e.getNumField("m_bastmode").intValue();
		this.m_asts = e.getNumField("m_asts").intValue();
		this.m_result = e.getNumField("m_result").intValue();

		this.m_extra = e.getField("m_extra").getData();
	}

	public String keys()
	{
		return "h_seq=%d, res_name=%s, uptime_ns=%s, " +
		       "h_version=0x08%x, " +
		       "h_lockspace=0x08%x, " +
		       "h_nodeid=%d, " +
		       //"h_length=%d, " +
		       //"h_cmd=%d, " +
		       "m_type=%d, " +
		       "m_nodeid=%d, " +
		       "m_pid=%d, " +
		       "m_lkid=%d, " +
		       "m_remid=%d, " +
		       "m_parent_lkid=%d, " +
		       "m_parent_remid=%d, " +
		       "m_exflags=0x08%x, " +
		       "m_sbflags=0x08%x, " +
		       "m_flags=0x08%x, " +
		       "m_lvbseq=%d, " +
		       "m_hash=%d, " +
		       "m_status=%d, " +
		       "m_grmode=%d, " +
		       "m_rqmode=%d, " +
		       "m_bastmode=%d, " +
		       "m_asts=%d, " +
		       "m_result=%d";
	}

        public byte[] values()
        {
		ByteBuffer bb;
		String s;

		bb = ByteBuffer.allocate(4 +
				2 + this.res.getResName().getBytes().length +
				2 + this.getTs().toString().getBytes().length + // uptime_ns
				4 + // h_version
				4 + // h_lockspace
				4 + // h_nodeid
				//2 + // h_length
				//1 + // h_cmd
				4 + // m_type
				4 + // m_nodeid
				4 + // m_pid
				4 + // m_lkid
				4 + // m_remid
				4 + // m_parent_lkid
				4 + // m_parent_remid
				4 + // m_exflags
				4 + // m_sbflags
				4 + // m_flags
				4 + // m_lvbseq
				4 + // m_hash
				4 + // m_status
				4 + // m_grmode
				4 + // m_rqmode
				4 + // m_bastmode
				4 + // m_asts
				4); // m_result


		bb.putInt(this.h_seq);

		s = this.res.getResName();
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		s = this.getTs().toString();
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		bb.putInt(this.h_version);
		bb.putInt(this.h_lockspace);
		bb.putInt(this.h_nodeid);
		//bb.putShort(this.h_length.shortValue());
		//bb.put(this.h_cmd);
		bb.putInt(this.m_type);
		bb.putInt(this.m_nodeid);
		bb.putInt(this.m_pid);
		bb.putInt(this.m_lkid);
		bb.putInt(this.m_remid);
		bb.putInt(this.m_parent_lkid);
		bb.putInt(this.m_parent_remid);
		bb.putInt(this.m_exflags);
		bb.putInt(this.m_sbflags);
		bb.putInt(this.m_flags);
		bb.putInt(this.m_lvbseq);
		bb.putInt(this.m_hash);
		bb.putInt(this.m_status);
		bb.putInt(this.m_grmode);
		bb.putInt(this.m_rqmode);
		bb.putInt(this.m_bastmode);
		bb.putInt(this.m_asts);
		bb.putInt(this.m_result);

		return bb.array();
        }

	public Integer getLineID()
	{
		return this.line_id;
	}

	public DLMResource getResource()
	{
		return this.res;
	}

	public BigInteger getTs()
	{
		return this.ts;
	}

	public Boolean isRequest()
	{
		return this.m_type == 1;
	}

	public Boolean isConvert()
	{
		return this.m_type == 2;
	}

	public Boolean isBast()
	{
		return this.m_type == 10;
	}

	public Boolean isLookup()
	{
		return this.m_type == 11;
	}

	public Boolean isRemove()
	{
		return this.m_type == 12;
	}

	public Integer getType()
	{
		return this.m_type;
	}

	public Integer getHNodeId()
	{
		return this.h_nodeid;
	}

	public Integer getNodeId()
	{
		return this.nodeid;
	}

	public Integer getMType()
	{
		return this.m_type;
	}

	public Byte[] getExtra()
	{
		return this.m_extra;
	}

	public Integer getLkbId()
	{
		return this.m_lkid;
	}

	public Integer getLsId()
	{
		return this.h_lockspace;
	}

	@Override
	public String toString()
	{
		//return String.format("%d %d %d %d %d", this.m_type, this.h_lockspace, this.m_lkid, this.m_remid, this.nodeid);
		return String.format("%d %d %d %d %d %s", this.m_type, this.h_lockspace, this.m_lkid, this.m_remid, this.nodeid,
				     DLMHelpers.getDataString(this.m_extra));
	}
}
