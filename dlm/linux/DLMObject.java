// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

interface Primitvible
{
	public Primitive getPrimitive();
}

public abstract class DLMObject implements Comparable<DLMObject>, Primitvible
{
	private BigInteger ts;
	protected Category c;

	public DLMObject(BigInteger ts)
	{
		this.ts = ts;
	}

	public BigInteger getTs()
	{
		return this.ts;
	}

	@Override
	public int compareTo(DLMObject o)
	{
		return this.ts.compareTo(o.ts);
	}
}
