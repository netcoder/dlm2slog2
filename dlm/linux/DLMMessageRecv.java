// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public class DLMMessageRecv extends DLMMessage
{
	public DLMMessageRecv(DLMTraceCmdEvent e, DLMMessageSend smsg)
	{
		super(e, smsg.getResource());
	}
}
