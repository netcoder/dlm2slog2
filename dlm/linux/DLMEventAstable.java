// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.util.*;
import java.nio.*;

public abstract class DLMEventAstable extends DLMEvent
{
	private DLMEventAst ast;
        private Integer flags;
        private Integer error;

	public DLMEventAstable(TraceCmdEvent e_start, TraceCmdEvent e_end, Integer nodeid)
        {
		super(e_start, nodeid);

		this.flags = e_start.getNumField("flags").intValue();
		this.error = e_end.getNumField("error").intValue();
        }

	@Override
	public String keys()
	{
		return super.keys() + ", flags=%s, error=%d";
	}

	@Override
        public byte[] values()
        {
		ByteBuffer bb;
		byte[] sbytes;

		String flagsstr = DLMHelpers.getFlagsString(this.flags);
		sbytes = super.values();
		bb = ByteBuffer.allocate(sbytes.length + 2 + flagsstr.getBytes().length + 4);
		bb.put(sbytes);

		bb.putShort((short)flagsstr.getBytes().length);
		bb.put(flagsstr.getBytes());
		bb.putInt(this.error);

		return bb.array();
        }

	public Integer getError()
	{
		return this.error;
	}

	public Integer getFlags()
	{
		return this.flags;
	}

	public Boolean isSuccess()
	{
		return this.error == 0;
	}

	public Boolean hasAst()
	{
		return this.ast != null;
	}

	public DLMEventAst getAst()
	{
		return this.ast;
	}

	public void setAst(DLMEventAst ast) throws RuntimeException
	{
		if (this.hasAst())
			throw new RuntimeException("found two asts");

		ast.setCaller(this);
		this.ast = ast;
	}
}
