// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public abstract class DLMEvent extends DLMObject
{
	private DLMResource res;
	private Integer line_id;
	private Integer lkb_id;
	private Integer nodeid;
	private String comm;
	private Integer pid;
	private Integer cpu;

	public DLMEvent(TraceCmdEvent e, Integer nodeid)
	{
		super(e.getTs());

		this.res = new DLMResource(e);
		this.line_id = Objects.hash(this.res.hashCode(), nodeid);
		this.lkb_id = e.getNumField("lkb_id").intValue();
		this.nodeid = nodeid;
		this.cpu = e.getCpu();
		this.pid = e.getPid();
		this.comm = e.getComm();
	}

	public Primitive getPrimitive()
	{
		Primitive p;

		/* TODO Category should be set, should be solved in different way */
		if (this.c == null)
			System.out.println("BUG");

		p = new Primitive(this.c, 1);
		p.setVertex(0, new Coord(DLMHelpers.getSecs(this.getTs()), this.line_id));
		this.c.setInfoKeys(this.keys());
                p.setInfoBuffer(this.values());

		return p;
	}

	public Integer getLineID()
        {
                return this.line_id;
        }

	public Integer getPid()
        {
                return this.pid;
        }

	public Integer getCpu()
        {
                return this.cpu;
        }

	public String keys()
	{
		return "pid=%d, comm=%s, cpu=%d, ls=0x08%x, lkb=0x%x, res_name=\"%s\", uptime_ns=%s";
	}

        public byte[] values()
        {
		ByteBuffer bb;
		String s;

		bb = ByteBuffer.allocate(4 + 2 + this.comm.getBytes().length + 4 + 4 + 4 + 2 + this.res.getResName().getBytes().length + 2 + this.getTs().toString().getBytes().length);

		bb.putInt(this.pid);

		s = this.comm;
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		bb.putInt(this.cpu);
		bb.putInt(this.res.getLsId());
		bb.putInt(this.lkb_id);

		s = this.res.getResName();
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		s = this.getTs().toString();
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		return bb.array();
        }

	public Integer getLsId()
	{
		return this.res.getLsId();
	}

	public Integer getLkbId()
	{
		return this.lkb_id;
	}

	public String getResName()
	{
		return this.res.getResName();
	}
}
