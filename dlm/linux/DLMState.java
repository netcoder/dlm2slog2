// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public class DLMState extends DLMObject
{
	private Integer mode;
	private BigInteger from;
	private BigInteger to;
	private Integer line_id;
	private Integer ls_id;
	private Integer lkb_id;
	private String res_name;

	public DLMState(Integer mode, BigInteger from, BigInteger to, Integer line_id,
			Integer ls_id, Integer lkb_id, String res_name)
	{
		super(to);

		this.mode = mode;
		this.from = from;
		this.to = to;
		this.line_id = line_id;
		this.ls_id = ls_id;
		this.lkb_id = lkb_id;
		this.res_name = res_name;

		this.c = DLMCategory.categories.get(DLMCategory.getStateType(mode));
	}

	public DLMState(Integer mode, DLMEvent e0, DLMEvent e1)
	{
		super(e1.getTs());

		this.mode = mode;
		this.from = e0.getTs();
		this.to = e1.getTs();
		this.line_id = e0.getLineID();
		this.ls_id = e0.getLsId();
		this.lkb_id = e0.getLkbId();
		this.res_name = e0.getResName();

		this.c = DLMCategory.categories.get(DLMCategory.getStateType(mode));
	}

	public String keys()
	{
		return "ls=0x08%x, lkb=0x%x, res_name=\"%s\"";
	}

        public byte[] values()
        {
		ByteBuffer bb;
		String s;

		bb = ByteBuffer.allocate(4 + 4 + 2 + this.res_name.getBytes().length);

		bb.putInt(this.ls_id);
		bb.putInt(this.lkb_id);

		s = this.res_name;
		bb.putShort((short)s.length());
		bb.put(s.getBytes());

		return bb.array();
        }

	public Primitive getPrimitive()
	{
		Primitive p;

		/* TODO Category should be set, should be solved in different way */
		if (this.c == null)
			System.out.println("BUG");

		p = new Primitive(this.c, 2);
		p.setVertex(0, new Coord(DLMHelpers.getSecs(from), line_id));
		p.setVertex(1, new Coord(DLMHelpers.getSecs(to), line_id));
		this.c.setInfoKeys(this.keys());
                p.setInfoBuffer(this.values());

		return p;
	}
}
