// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import java.util.*;
import java.nio.*;

import tracecmd.*;

public class DLMEventGeneric extends DLMEvent
{
	public DLMEventGeneric(TraceCmdEvent e, Integer nodeid,
			       DLMCategory.Type type)
	{
		super(e, nodeid);

		this.c = DLMCategory.categories.get(type);
	}
}
