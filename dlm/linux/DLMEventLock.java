// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import java.util.*;
import java.nio.*;

import tracecmd.*;

public class DLMEventLock extends DLMEventAstable
{
	private ArrayList<DLMEventBast> basts;
        private Integer mode;

        public DLMEventLock(TraceCmdEvent e_start, TraceCmdEvent e_end, Integer nodeid)
        {
		super(e_start, e_end, nodeid);

		this.mode = e_start.getNumField("mode").intValue();
		this.basts = new ArrayList<DLMEventBast>();

		if (this.isSuccess())
			this.c = DLMCategory.categories.get(DLMCategory.Type.LOCK);
		else
			this.c = DLMCategory.categories.get(DLMCategory.Type.LOCK_ERROR);
        }

	public Integer getMode()
	{
		return this.mode;
	}

	public Boolean isConvert()
	{
		return (this.mode & 0x4) == 0x4;
	}

	public Boolean hasBasts()
	{
		return this.basts.size() != 0;
	}

	@Override
	public String keys()
	{
		return super.keys() + ", mode=%s";
	}

	@Override
        public byte[] values()
        {
		ByteBuffer bb;
		byte[] sbytes;

		String modestr = DLMHelpers.getModeString(this.mode);
		sbytes = super.values();
		bb = ByteBuffer.allocate(sbytes.length + 4);
		bb.put(sbytes);

		bb.putShort((short)modestr.getBytes().length);
		bb.put(modestr.getBytes());

		return bb.array();
        }

	public void addBastEvent(DLMEventBast bast)
	{
		this.basts.add(bast);
	}

	public ArrayList<DLMEventBast> getBasts()
	{
		return this.basts;
	}
}
