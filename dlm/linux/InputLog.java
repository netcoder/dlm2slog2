// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import dlm.slog2.output.*;
import dlm.gui.*;
import logformat.trace.*;
import base.drawable.*;
import java.math.*;
import java.util.*;
import tracecmd.*;
import javax.swing.*;

/*
   This class provides the Java version of TRACE-API.
   */
public class InputLog
{
	/* LineIDs */
	private Set<DLMLineID> resYSet;
	/* nodeid -> TraceCmd */
	private Map<Integer, TraceCmd> traces;
	/* LineID -> ordered List<DLMEvent> */
	private Map<Integer, List<DLMEvent>> line_events;
	/* slog2 sorted drawable creation */
	private List<DLMObject> draws;

	private JProgressBar progressBar;

	private BigInteger last_ts;

	private List<Object> ds;
	private Iterator<Object> dit;
	private Object i;

	private int num_topology_returned;
	private int count;

	public InputLog(JProgressBar progressBar)
	{
		this.progressBar = progressBar;
		this.resYSet = new HashSet<DLMLineID>();
		this.traces = new HashMap<Integer, TraceCmd>();
		this.line_events = new HashMap<Integer, List<DLMEvent>>();
		this.draws = new ArrayList<DLMObject>();
	}

	public Map<DLMResource, DLMResource> getResourceNames(Map<Integer, String> files)
	{
		Map<DLMResource, DLMResource> res_set = new HashMap<DLMResource, DLMResource>();

		this.last_ts = null;
		this.traces.clear();

		int i = 1;
		this.progressBar.setValue(0);
		this.progressBar.paintImmediately(this.progressBar.getBounds());
		for (Map.Entry<Integer, String> f:files.entrySet()) {
			traces.put(f.getKey(), new TraceCmd(f.getValue()));
		}

		for (Map.Entry<Integer, TraceCmd> ks:traces.entrySet()) {
			for (TraceCmdEvent e:ks.getValue()) {
				DLMResource res, r;

				if (!(DLMHelpers.isDLMEventLockStart(e) ||
				      DLMHelpers.isDLMEventLockEnd(e) ||
				      DLMHelpers.isDLMEventUnLockStart(e) ||
				      DLMHelpers.isDLMEventUnLockEnd(e) ||
				      DLMHelpers.isDLMEventBast(e) ||
				      DLMHelpers.isDLMEventAst(e)))
					continue;

				res = new DLMResource(e);
				r = res_set.get(res);
				if (r == null)
					res_set.put(res, res);
				else
					res = r;

				res.incNumEvents();
			}

			this.progressBar.setValue((i*100)/files.size());
			this.progressBar.paintImmediately(this.progressBar.getBounds());
			i+=1;
		}

		return res_set;
	}

	public void parseLog(Set<Integer> nodes, Set<DLMResource> filter)
	{
		Map<DLMCall, TraceCmdEvent> unlock_start_calls = new HashMap<DLMCall, TraceCmdEvent>();
		Map<DLMCall, TraceCmdEvent> lock_start_calls = new HashMap<DLMCall, TraceCmdEvent>();
		DLMEvent de = null;
		DLMResource res;

		this.line_events.clear();
		this.resYSet.clear();
		this.draws.clear();

		/* TODO cleanup this */
		/* fill possible YCoordMap mapping */
		for (Map.Entry<Integer, TraceCmd> ks:traces.entrySet()) {
			Integer nodeid = ks.getKey();

			for (TraceCmdEvent e:ks.getValue()) {
				if (DLMHelpers.isDLMEventLockStart(e) ||
				    DLMHelpers.isDLMEventLockEnd(e) ||
				    DLMHelpers.isDLMEventUnLockStart(e) ||
				    DLMHelpers.isDLMEventUnLockEnd(e) ||
				    DLMHelpers.isDLMEventBast(e) ||
				    DLMHelpers.isDLMEventAst(e)) {
					DLMResource tmp = new DLMResource(e.getNumField("ls_id").intValue(), DLMHelpers.getDLMResName(e));
					if (!filter.contains(tmp))
						continue;

					DLMLineID lineid = new DLMLineID(tmp, nodeid);
					this.resYSet.add(lineid);

					/* only way for now to get last timestamp */
					if (this.last_ts == null ||
					    e.getTs().compareTo(this.last_ts) > 0)
						this.last_ts = e.getTs();
				}

				if (DLMHelpers.isDLMSendMessage(e)) {
					DLMResource tmp = new DLMResource(e.getNumField("h_lockspace").intValue(), DLMHelpers.getDLMResName(e));
					if (!filter.contains(tmp))
						continue;

					DLMLineID lineid = new DLMLineID(tmp, nodeid);
					this.resYSet.add(lineid);

					lineid = new DLMLineID(tmp, e.getNumField("h_nodeid").intValue());
					this.resYSet.add(lineid);
				}
			}
		}

		int iter = 1;
		this.progressBar.setValue(0);
		this.progressBar.paintImmediately(this.progressBar.getBounds());
		for (Map.Entry<Integer, TraceCmd> ks:traces.entrySet()) {
			Integer nodeid = ks.getKey();

			unlock_start_calls.clear();
			lock_start_calls.clear();

			for (TraceCmdEvent e:ks.getValue()) {
				if (!(DLMHelpers.isDLMEventLockStart(e) ||
				      DLMHelpers.isDLMEventLockEnd(e) ||
				      DLMHelpers.isDLMEventUnLockStart(e) ||
				      DLMHelpers.isDLMEventUnLockEnd(e) ||
				      DLMHelpers.isDLMEventBast(e) ||
				      DLMHelpers.isDLMEventAst(e)))
					continue;

				res = new DLMResource(e);
				if (!filter.contains(res))
					continue;

				if (DLMHelpers.isDLMEventLockStart(e)) {
					lock_start_calls.put(new DLMCall(e), e);
					continue;
				} else if (DLMHelpers.isDLMEventLockEnd(e)) {
					TraceCmdEvent lock_start;

					lock_start = lock_start_calls.remove(new DLMCall(e));
					if (lock_start == null) {
						System.out.println("WARNING no DLMLockStart: " + e.getTs() + " " + DLMHelpers.getDLMResName(e));
						continue;
					}

					this.draws.add(new DLMEventGeneric(e, nodeid, DLMCategory.Type.LOCK_END));
					de = new DLMEventLock(lock_start, e, nodeid);
				} else if (DLMHelpers.isDLMEventUnLockStart(e)) {
					unlock_start_calls.put(new DLMCall(e), e);
					continue;
				} else if (DLMHelpers.isDLMEventUnLockEnd(e)) {
					TraceCmdEvent unlock_start;
					DLMEventUnLock unlock;

					unlock_start = unlock_start_calls.remove(new DLMCall(e));
					if (unlock_start == null) {
						System.out.println("WARNING no DLMUnLockStart: " + e.getTs());
						continue;
					}

					unlock = new DLMEventUnLock(unlock_start, e, nodeid);
					if (unlock.isCancel())
						this.draws.add(new DLMEventGeneric(e, nodeid, DLMCategory.Type.CANCEL_END));
					else
						this.draws.add(new DLMEventGeneric(e, nodeid, DLMCategory.Type.UNLOCK_END));

					de = unlock;
				} else if (DLMHelpers.isDLMEventBast(e)) {
					de = new DLMEventBast(e, nodeid);
				} else if (DLMHelpers.isDLMEventAst(e)) {
					de = new DLMEventAst(e, nodeid);
				}

				List<DLMEvent> etmp = this.line_events.get(de.getLineID());
				if (etmp == null) {
					etmp = new ArrayList<DLMEvent>();
					this.line_events.put(de.getLineID(), etmp);
				}

				etmp.add(de);
				this.draws.add(de);
			}

			this.progressBar.setValue((iter*100)/traces.size());
			this.progressBar.paintImmediately(this.progressBar.getBounds());
			iter += 1;
		}

		this.ds = new LinkedList<Object>();

		for (Category c:DLMCategory.categories.values()) {
			this.ds.add(c);
		}

		this.progressBar.setValue(0);
		this.progressBar.paintImmediately(this.progressBar.getBounds());
		iter = 1;
		for (Map.Entry<Integer, List<DLMEvent>> line:this.line_events.entrySet()) {
			DLMEventLock last_lock = null, last_last_lock = null, last_last_successful_lock = null;
			DLMEventUnLock last_unlock = null;
			DLMEventAst last_ast = null;

			/* We need to reorder the list because the ast event can be
			 * between start/stop events of e.g. dlm_lock. We add the
			 * ast event directly and the request event at stop but using
			 * the timestamp of start to avoid this specific race.
			 *
			 * However the list is not sorted anymore as the events occcur
			 * in chronological order. To fix it, we simple sort the events
			 * again according their timestamps.
			 */
			Collections.sort(line.getValue());
			for (DLMEvent e:line.getValue()) {
				if (e instanceof DLMEventLock) {
					DLMEventLock lock = (DLMEventLock)e;

					if (lock.isSuccess())
						last_lock = lock;
				} else if (e instanceof DLMEventUnLock) {
					DLMEventUnLock unlock = (DLMEventUnLock)e;

					if (unlock.isSuccess()) {
						/* ignore only ast matters */
						if (unlock.isCancel())
							continue;

						last_unlock = unlock;
					}
				} else if (e instanceof DLMEventBast) {
					DLMEventBast bast = (DLMEventBast)e;

					if (last_lock != null)
						last_lock.addBastEvent(bast);
				} else if (e instanceof DLMEventAst) {
					DLMEventAst ast = (DLMEventAst)e;

					/* TODO cleanup between last_lock and last_ast.getCaller()
					 * they should be the same? Also some code share is possible
					 */
					if (last_lock != null && last_unlock != null) {
						/* should never happen indicates wrong dlm use */
						System.out.println("Invalid state");
						continue;
					} else if (last_lock != null) {
						if (last_ast != null && last_last_lock != null) {
							this.draws.add(new DLMState(-1, last_last_lock, last_ast));

							if (last_ast.isSuccessLock()) {
								this.draws.add(new DLMState(last_last_lock.getMode(), last_ast, last_lock));
								last_last_successful_lock = last_last_lock;
							} else {
								if (last_last_successful_lock != null)
									this.draws.add(new DLMState(last_last_successful_lock.getMode(), last_ast, last_lock));
							}
						}

						/* last handling */
						last_last_lock = last_lock;
						last_lock.setAst(ast);
						last_lock = null;
						last_ast = ast;
					} else if (last_unlock != null) {
						/* doesn't matter if fails or not */
						this.draws.add(new DLMState(-1, last_unlock, ast));

						if (last_ast != null) {
							DLMEventAstable astable;

							astable = last_ast.getCaller();
							if (astable instanceof DLMEventLock) {
								DLMEventLock lock = (DLMEventLock)astable;

								this.draws.add(new DLMState(-1, lock, last_ast));
								if (last_ast.isSuccessLock()) {
									this.draws.add(new DLMState(lock.getMode(), last_ast, last_unlock));
									last_last_successful_lock = lock;
								} else {
									if (last_last_successful_lock != null)
										this.draws.add(new DLMState(last_last_successful_lock.getMode(), last_ast, last_unlock));
								}
							}
						}

						/* go back to the last last lock call as last lock */
						if (ast.isCancel()) {
							last_lock = last_last_lock;
						} else if (ast.isSuccessUnLock()) {
							last_last_successful_lock = null;
							last_last_lock = null;
						}

						/* last handling */
						last_unlock.setAst(ast);
						last_unlock = null;
						last_ast = ast;
					}

				}
			}

			/* last state until trace end */
			if (last_ast != null && last_ast.isSuccessLock()) {
				DLMEventAstable astable;

				astable = last_ast.getCaller();
				if (astable instanceof DLMEventLock) {
					DLMEventLock lock = (DLMEventLock)astable;

					this.draws.add(new DLMState(-1, lock, last_ast));
					if (last_lock != null) {
						this.draws.add(new DLMState(lock.getMode(), last_ast, last_lock));
						this.draws.add(new DLMState(-1, last_lock.getTs(),
									    this.last_ts, last_lock.getLineID(),
									    last_lock.getLsId(), last_lock.getLkbId(),
									    last_lock.getResName()));
					} else {
						this.draws.add(new DLMState(lock.getMode(), last_ast.getTs(),
									    this.last_ts, last_ast.getLineID(),
									    last_ast.getLsId(), last_ast.getLkbId(),
									    last_ast.getResName()));
					}
				}
			}

			this.progressBar.setValue((iter*100)/this.line_events.size());
			this.progressBar.paintImmediately(this.progressBar.getBounds());
			iter += 1;
		}

		/* arrows only handling */
		List<DLMTraceCmdEvent> arrow_events = new LinkedList<DLMTraceCmdEvent>();
		for (Map.Entry<Integer, TraceCmd> ks:traces.entrySet()) {
			for (TraceCmdEvent e:ks.getValue()) {
				if (!(DLMHelpers.isDLMSendMessage(e) ||
				      DLMHelpers.isDLMRecvMessage(e)))
					continue;

				arrow_events.add(new DLMTraceCmdEvent(e, ks.getKey()));
			}
		}

		Collections.sort(arrow_events);

		Map<Integer, DLMMessageSend> msgs = new HashMap<Integer, DLMMessageSend>();
		for (DLMTraceCmdEvent e:arrow_events) {
			Integer h_nodeid = e.getNumField("h_nodeid").intValue();
			Integer dst = e.getNumField("dst").intValue();
			Integer seq = e.getNumField("h_seq").intValue();
			Integer seqhash = Objects.hash(seq, h_nodeid, dst);

			if (DLMHelpers.isDLMSendMessage(e)) {
				DLMMessageSend smsg = new DLMMessageSend(e);
				if (!filter.contains(smsg.getResource()))
					continue;

				msgs.put(seqhash, smsg);
				continue;
			} else if (DLMHelpers.isDLMRecvMessage(e)) {
				DLMMessageSend smsg = msgs.remove(seqhash);
				if (smsg != null) {
					DLMMessageRecv rmsg = new DLMMessageRecv(e, smsg);
					this.draws.add(new DLMArrow(smsg, rmsg));
				}
				continue;
			}
		}

		/* slog2 does like a specific order for adding Primitives */
		Collections.sort(this.draws);
		for (DLMObject e:this.draws) {
			this.ds.add(e.getPrimitive());
		}

		YCoordMap map = this.create_ymap();
		this.ds.add(map);

		this.dit = this.ds.iterator();
	}

	private YCoordMap create_ymap()
	{
		List<Integer> ylist = new ArrayList<Integer>();
		Integer amount = 0;
		String cols[] = new String[3];
		cols[0] = "Lockspace";
		cols[1] = "Resource";
		cols[2] = "Node";

		amount = this.resYSet.size();

		int[] arr = new int[amount * 4];
		int i = 0;
		for (DLMLineID l:this.resYSet) {
			arr[i] = l.hashCode();
			arr[i + 1] = l.getResource().getLsId();
			arr[i + 2] = l.getResource().getResName().hashCode();
			arr[i + 3] = l.getNodeId();
			i = i+4;
		}

		return new YCoordMap(amount, 4, "DLM", cols, arr, null);
	}

	public Composite getNextComposite()
	{
		return (Composite)this.i;
	}

	public Primitive getNextPrimitive()
	{
		return (Primitive)this.i;
	}

	public YCoordMap getNextYCoordMap()
	{
		return (YCoordMap)this.i;
	}

	public Category getNextCategory()
	{
		return (Category)this.i;
	}

	public boolean close()
	{
		return true;
	}

	public boolean open()
	{
		return true;
	}

	public int peekNextKindIndex()
	{
		if (this.dit.hasNext())
			this.i = this.dit.next();
		else
			return Kind.EOF_ID;

		if (this.i instanceof Category)
			return Kind.CATEGORY_ID;
		if (this.i instanceof Composite)
			return Kind.COMPOSITE_ID;
		if (this.i instanceof Primitive)
			return Kind.PRIMITIVE_ID;
		if (this.i instanceof YCoordMap)
			return Kind.YCOORDMAP_ID;
		else
			return Kind.EOF_ID;
	}

	public Kind peekNextKind()
	{
		// Return all the Topology names.
		if ( num_topology_returned < 3 )
			return Kind.TOPOLOGY;

		int next_kind_index  = this.peekNextKindIndex();
		switch ( next_kind_index ) {
			case Kind.TOPOLOGY_ID :
				return Kind.TOPOLOGY;
			case Kind.EOF_ID :
				return Kind.EOF;
			case Kind.PRIMITIVE_ID :
				return Kind.PRIMITIVE;
			case Kind.COMPOSITE_ID :
				return Kind.COMPOSITE;
			case Kind.CATEGORY_ID :
				return Kind.CATEGORY;
			case Kind.YCOORDMAP_ID :
				return Kind.YCOORDMAP;
			default :
				System.err.println( "trace.InputLog.peekNextKind(): "
						+ "Unknown value, " + next_kind_index );
		}

		return null;
	}

	public Topology getNextTopology()
	{
		switch ( num_topology_returned ) {
			case 0:
				num_topology_returned = 1;
				return Topology.EVENT;
			case 1:
				num_topology_returned = 2;
				return Topology.STATE;
			case 2:
				num_topology_returned = 3;
				return Topology.ARROW;
			default:
				System.err.println( "All Topology Names have been returned" );
		}

		return null;
	}
}
