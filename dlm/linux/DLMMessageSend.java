// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.nio.*;
import java.util.*;
import java.math.*;

public class DLMMessageSend extends DLMMessage
{
	public DLMMessageSend(DLMTraceCmdEvent e)
	{
		super(e, new DLMResource(e.getNumField("h_lockspace").intValue(),
					 DLMHelpers.getDLMResName(e)));

	}
}
