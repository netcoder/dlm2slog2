// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.util.*;
import java.nio.*;

public class DLMEventAst extends DLMEvent
{
	private DLMEventAstable caller;
	private Integer sb_status;
	private Byte sb_flags;

	public DLMEventAst(TraceCmdEvent e, Integer nodeid)
	{
		super(e, nodeid);

		this.sb_status = e.getNumField("sb_status").intValue();
		this.sb_flags = e.getNumField("sb_flags").byteValue();

		if (this.isSuccessLock() || this.isSuccessUnLock())
			this.c = DLMCategory.categories.get(DLMCategory.Type.AST);
		else if (this.isCancel())
			this.c = DLMCategory.categories.get(DLMCategory.Type.AST_ECANCEL);
		else if (this.isAgain())
			this.c = DLMCategory.categories.get(DLMCategory.Type.AST_EAGAIN);
		else if (this.isDeadlock())
			this.c = DLMCategory.categories.get(DLMCategory.Type.AST_EDEADLK);
		else
			this.c = DLMCategory.categories.get(DLMCategory.Type.AST_ERROR);
	}

	public Boolean isSuccessLock()
	{
		switch (this.sb_status) {
		case 0:
			return true;
		default:
			return false;
		}
	}

	public Boolean isSuccessUnLock()
	{
		switch (this.sb_status) {
		case -65538:
			return true;
		default:
			return false;
		}
	}

	public Boolean isAgain()
	{
		switch (this.sb_status) {
		case -11:
			return true;
		default:
			return false;
		}
	}

	public Boolean isDeadlock()
	{
		switch (this.sb_status) {
		case -35:
			return true;
		default:
			return false;
		}
	}

	public Boolean isCancel()
	{
		switch (this.sb_status) {
		case -65537:
			return true;
		default:
			return false;
		}
	}

	@Override
	public String keys()
	{
		return super.keys() + ", sb_status=%d, sb_flags=%s";
	}

	@Override
        public byte[] values()
        {
		ByteBuffer bb;
		byte[] sbytes;

		String flagsstr = DLMHelpers.getSbFlagsString(this.sb_flags);
		sbytes = super.values();
		bb = ByteBuffer.allocate(sbytes.length + 4 + 2 + flagsstr.getBytes().length);
		bb.put(sbytes);

		bb.putInt(this.sb_status);

		bb.putShort((short)flagsstr.getBytes().length);
		bb.put(flagsstr.getBytes());

		return bb.array();
        }

	@Override
        public String toString()
        {
                return "call=ast, " + super.toString() + ", sb_status=" + this.sb_status;
        }

	public void setCaller(DLMEventAstable caller)
	{
		if (this.caller != null)
			throw new RuntimeException("found two callers for ast");

		this.caller = caller;
	}

	public DLMEventAstable getCaller()
	{
		return this.caller;
	}
}
