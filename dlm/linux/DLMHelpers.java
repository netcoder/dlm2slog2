// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import java.nio.charset.StandardCharsets;
import base.drawable.*;
import logformat.trace.*;
import java.util.*;
import java.math.*;
import tracecmd.*;

public class DLMHelpers
{
	/*
	 * Taken from Linux kernel.
	 *
	 * Lock compatibilty matrix - thanks Steve
	 * UN = Unlocked state. Not really a state, used as a flag
	 * PD = Padding. Used to make the matrix a nice power of two in size
	 * Other states are the same as the VMS DLM.
	 * Usage: matrix[grmode+1][rqmode+1]  (although m[rq+1][gr+1] is the same)
	 */
	static private final int[][] __dlm_compat_matrix = {
		/* UN NL CR CW PR PW EX PD */
		  {1, 1, 1, 1, 1, 1, 1, 0}, /* UN */
		  {1, 1, 1, 1, 1, 1, 1, 0}, /* NL */
		  {1, 1, 1, 1, 1, 1, 0, 0}, /* CR */
		  {1, 1, 1, 1, 0, 0, 0, 0}, /* CW */
		  {1, 1, 1, 0, 1, 0, 0, 0}, /* PR */
		  {1, 1, 1, 0, 0, 0, 0, 0}, /* PW */
		  {1, 1, 0, 0, 0, 0, 0, 0}, /* EX */
		  {0, 0, 0, 0, 0, 0, 0, 0}  /* PD */
	};

	public static Boolean isDLMModeCompatible(Integer grmode, Integer rqmode)
	{
		return DLMHelpers.__dlm_compat_matrix[grmode + 1][rqmode + 1] == 1;
	}

	/* should be used to detect start/end not taking care of nodeid */
	public static Integer entry_hash(TraceCmdEvent e)
	{
		Integer lkb_id, ls_id;

		ls_id = e.getNumField("ls_id").intValue();
		lkb_id = e.getNumField("lkb_id").intValue();

		return Objects.hash(ls_id, lkb_id, e.getPid());
	}

	public static Boolean isDLMEventLockStart(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_lock_start");
	}

	public static Boolean isDLMEventLockEnd(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_lock_end");
	}

	public static Boolean isDLMEventUnLockStart(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_unlock_start");
	}

	public static Boolean isDLMEventUnLockEnd(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_unlock_end");
	}

	public static Boolean isDLMEventBast(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_bast");
	}

	public static Boolean isDLMEventAst(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_ast");
	}

	public static Boolean isDLMSendMessage(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_send_message");
	}

	public static Boolean isDLMRecvMessage(TraceCmdEvent e)
	{
		return e.getSystem().equals("dlm") && e.getName().equals("dlm_recv_message");
	}

	public static byte[] HexStrToByte(String str)
	{
		byte[] ans = new byte[str.length() / 2];

		for (int i = 0; i < ans.length; i++) {
			int index = i * 2;

			int val = Integer.parseUnsignedInt(str.substring(index, index + 2), 16);
			ans[i] = (byte)val;
		}

		return ans;
	}

	public Boolean isLKFlagCancel(int flags)
	{
		return ((flags & 0x2) == 0x2);
	}

	public static char asciiToChar(byte ascii)
	{
		if (ascii < 0 || ascii >= 127)
			throw new IllegalArgumentException("Invalid ASCII value!");

		return (char)ascii;
	}

	public static String getDataString(Byte[] bs)
	{
		byte[] bytes = new byte[bs.length];

		int j=0;
		for(Byte b: bs)
			bytes[j++] = b.byteValue();

		return new String(bytes, StandardCharsets.US_ASCII);
	}

	public static String __getDLMResName(TraceCmdEvent e)
	{
		Byte[] bs = e.getField("res_name").getData();
		byte[] bytes = new byte[bs.length];

		int j=0;
		for(Byte b: bs)
			bytes[j++] = b.byteValue();

		return new String(bytes, StandardCharsets.US_ASCII);
	}

	public static String getDLMResName(TraceCmdEvent e)
	{
		return __getDLMResName(e);
	}

	public static String getDLMResNameStr(String str)
	{
		String ret = new String();
		byte arr[];

		arr = HexStrToByte(str);
		for (byte c:arr) {
			try {
				ret += asciiToChar(c);
			} catch (Exception exc) {
				return str;
			}
		}

		return ret;
	}

	public static Double getSecs(BigInteger ts)
	{
		return new BigDecimal(ts).divide(new BigDecimal(1000000000)).doubleValue();
	}

	public static String getSbFlagsString(Byte flags)
	{
		boolean first = true;
		String ret = "";

		for (int i = 1 ; i <= flags; i<<=1) {
			if ((flags & i) == 0)
				continue;

			if (!first) {
				if ((flags & i) != 0)
					ret += " | ";
			} else {
				first = false;
			}

			switch (flags & i) {
			case 0x1:
				ret += "DEMOTED";
				break;
			case 0x2:
				ret += "VALNOTVALID";
				break;
			case 0x4:
				ret += "ALTMODE";
				break;
			default:
				ret += "UNKNOWN";
				break;
			}
		}

		return ret;
	}

	public static String getFlagsString(Integer flags)
	{
		boolean first = true;
		String ret = "";

		for (int i = 1 ; i <= flags; i<<=1) {
			if ((flags & i) == 0)
				continue;

			if (!first) {
				if ((flags & i) != 0)
					ret += " | ";
			} else {
				first = false;
			}

			switch (flags & i) {
			case 0x1:
				ret += "NOQUEUE";
				break;
			case 0x2:
				ret += "CANCEL";
				break;
			case 0x4:
				ret += "CONVERT";
				break;
			case 0x8:
				ret += "VALBLK";
				break;
			case 0x10:
				ret += "QUECVT";
				break;
			case 0x20:
				ret += "IVVALBLK";
				break;
			case 0x40:
				ret += "CONVDEADLK";
				break;
			case 0x80:
				ret += "PERSISTENT";
				break;
			case 0x100:
				ret += "NODLCKWT";
				break;
			case 0x200:
				ret += "NODLCKBLK";
				break;
			case 0x400:
				ret += "EXPEDITE";
				break;
			case 0x800:
				ret += "NOQUEUEBAST";
				break;
			case 0x1000:
				ret += "HEADQUE";
				break;
			case 0x2000:
				ret += "NOORDER";
				break;
			case 0x4000:
				ret += "ORPHAN";
				break;
			case 0x8000:
				ret += "ALTPR";
				break;
			case 0x10000:
				ret += "ALTCW";
				break;
			case 0x20000:
				ret += "FORCEUNLOCK";
				break;
			case 0x40000:
				ret += "TIMEOUT";
				break;
			default:
				ret += "UNKNOWN";
				break;
			}

		}

		return ret;
	}

	public static String getModeString(Integer mode)
	{
		switch (mode) {
		case -1:
			return "IV";
		case 0:
			return "NL";
		case 1:
			return "CR";
		case 2:
			return "CW";
		case 3:
			return "PR";
		case 4:
			return "PW";
		case 5:
			return "EX";
		default:
			return mode.toString();
		}
	}
}
