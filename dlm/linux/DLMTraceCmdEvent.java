// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import tracecmd.*;

public class DLMTraceCmdEvent extends TraceCmdEvent
{
	private Integer nodeid;

	public DLMTraceCmdEvent(TraceCmdEvent e, Integer nodeid)
	{
		super(e);

		this.nodeid = nodeid;
	}

	public Integer getNodeId()
	{
		return nodeid;
	}

	@Override
	public String toString()
	{
		return super.toString() + " nodeid: " + this.nodeid;
	}
}
