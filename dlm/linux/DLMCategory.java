// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import logformat.trace.*;
import java.awt.Color;
import java.util.*;

public class DLMCategory
{
	enum Type {
		LOCK,
		LOCK_END,
		LOCK_ERROR,
		UNLOCK,
		UNLOCK_END,
		UNLOCK_ERROR,
		CANCEL,
		CANCEL_END,
		CANCEL_ERROR,
		AST,
		AST_ECANCEL,
		AST_EAGAIN,
		AST_EDEADLK,
		AST_ERROR,
		BAST,
		PENDING,
		NL,
		CR,
		CW,
		PR,
		PW,
		EX,
		MSG_REQUEST,
		MSG_CONVERT,
		MSG_UNLOCK,
		MSG_CANCEL,
		MSG_REQUEST_REPLY,
		MSG_CONVERT_REPLY,
		MSG_UNLOCK_REPLY,
		MSG_CANCEL_REPLY,
		MSG_GRANT,
		MSG_BAST,
		MSG_LOOKUP,
		MSG_REMOVE,
		MSG_LOOKUP_REPLY,
	}

	public static DLMCategory.Type getStateType(int mode)
	{
		switch (mode + 1) {
		case 0:
			return DLMCategory.Type.PENDING;
		case 1:
			return DLMCategory.Type.NL;
		case 2:
			return DLMCategory.Type.CR;
		case 3:
			return DLMCategory.Type.CW;
		case 4:
			return DLMCategory.Type.PR;
		case 5:
			return DLMCategory.Type.PW;
		case 6:
			return DLMCategory.Type.EX;
		default:
			return null;
		}
	}

	private static Category createCategory(Type type, Topology topo, Integer color, Boolean visible)
	{
		int alpha = ColorAlpha.OPAQUE;
		Category c;

		if (topo.isState())
			alpha = ColorAlpha.HALF_OPAQUE;

		c = new Category(type.ordinal(), type.toString(), topo, new ColorAlpha(new Color(color), alpha), 1);
		c.setVisible(visible);
		return c;
	}

	public static Map<Type, Category> categories;
	static
	{
		/* colors from  http://tango.freedesktop.org/Tango_Icon_Theme_Guidelines */

		categories = new HashMap<Type, Category>();

		/* events */
		categories.put(Type.LOCK, createCategory(Type.LOCK, Topology.EVENT, 0x729fcf, true));
		categories.put(Type.LOCK_END, createCategory(Type.LOCK_END, Topology.EVENT, 0x000000, false));
		categories.put(Type.LOCK_ERROR, createCategory(Type.LOCK_ERROR, Topology.EVENT, 0xef2929, true));
		categories.put(Type.UNLOCK, createCategory(Type.UNLOCK, Topology.EVENT, 0xfcaf3e, true));
		categories.put(Type.UNLOCK_END, createCategory(Type.UNLOCK_END, Topology.EVENT, 0x000000, false));
		categories.put(Type.UNLOCK_ERROR, createCategory(Type.UNLOCK_ERROR, Topology.EVENT, 0xef2929, true));
		categories.put(Type.CANCEL, createCategory(Type.CANCEL, Topology.EVENT, 0xad7fa8, true));
		categories.put(Type.CANCEL_END, createCategory(Type.CANCEL_END, Topology.EVENT, 0x000000, false));
		categories.put(Type.CANCEL_ERROR, createCategory(Type.CANCEL_ERROR, Topology.EVENT, 0xef2929, true));
		categories.put(Type.AST, createCategory(Type.AST, Topology.EVENT, 0x8ae234, true));
		categories.put(Type.AST_ECANCEL, createCategory(Type.AST_ECANCEL, Topology.EVENT, 0x5c3566, true));
		categories.put(Type.AST_EAGAIN, createCategory(Type.AST_EAGAIN, Topology.EVENT, 0xf57900, true));
		categories.put(Type.AST_EDEADLK, createCategory(Type.AST_EDEADLK, Topology.EVENT, 0x8f5902, true));
		categories.put(Type.AST_ERROR, createCategory(Type.AST_ERROR, Topology.EVENT, 0xef2929, true));
		categories.put(Type.BAST, createCategory(Type.BAST, Topology.EVENT, 0xfce94f, true));

		/* states */
		categories.put(Type.PENDING, createCategory(Type.PENDING, Topology.STATE, 0x4e9a06, true));
		categories.put(Type.NL, createCategory(Type.NL, Topology.STATE, 0xbabdb6, true));
		categories.put(Type.CR, createCategory(Type.CR, Topology.STATE, 0xce5c00, true));
		categories.put(Type.CW, createCategory(Type.CW, Topology.STATE, 0x75507b, true));
		categories.put(Type.PR, createCategory(Type.PR, Topology.STATE, 0x204a87, true));
		categories.put(Type.PW, createCategory(Type.PW, Topology.STATE, 0x8f5902, true));
		categories.put(Type.EX, createCategory(Type.EX, Topology.STATE, 0xcc0000, true));

		/* arrows */
		categories.put(Type.MSG_REQUEST, createCategory(Type.MSG_REQUEST, Topology.ARROW, 0xfce94f, true));
		categories.put(Type.MSG_CONVERT, createCategory(Type.MSG_CONVERT, Topology.ARROW, 0xfcaf3e, true));
		categories.put(Type.MSG_UNLOCK, createCategory(Type.MSG_UNLOCK, Topology.ARROW, 0xe9b96e, true));
		categories.put(Type.MSG_CANCEL, createCategory(Type.MSG_CANCEL, Topology.ARROW, 0x8ae234, true));
		categories.put(Type.MSG_REQUEST_REPLY, createCategory(Type.MSG_REQUEST_REPLY, Topology.ARROW, 0x729fcf, true));
		categories.put(Type.MSG_CONVERT_REPLY, createCategory(Type.MSG_CONVERT_REPLY, Topology.ARROW, 0xad7fa8, true));
		categories.put(Type.MSG_UNLOCK_REPLY, createCategory(Type.MSG_UNLOCK_REPLY, Topology.ARROW, 0xef2929, true));
		categories.put(Type.MSG_CANCEL_REPLY, createCategory(Type.MSG_CANCEL_REPLY, Topology.ARROW, 0xc4a000, true));
		categories.put(Type.MSG_GRANT, createCategory(Type.MSG_GRANT, Topology.ARROW, 0xce5c00, true));
		categories.put(Type.MSG_BAST, createCategory(Type.MSG_BAST, Topology.ARROW, 0x8f5902, true));
		categories.put(Type.MSG_LOOKUP, createCategory(Type.MSG_LOOKUP, Topology.ARROW, 0x4e9a06, true));
		categories.put(Type.MSG_REMOVE, createCategory(Type.MSG_REMOVE, Topology.ARROW, 0xffffff, true));
		categories.put(Type.MSG_LOOKUP_REPLY, createCategory(Type.MSG_LOOKUP_REPLY, Topology.ARROW, 0x204a87, true));
	}
}
