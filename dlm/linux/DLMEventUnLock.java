// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import base.drawable.*;
import tracecmd.*;
import java.util.*;
import java.nio.*;

public class DLMEventUnLock extends DLMEventAstable
{
        public DLMEventUnLock(TraceCmdEvent e_start, TraceCmdEvent e_end, Integer nodeid)
        {
		super(e_start, e_end, nodeid);

		if (this.isCancel()) {
			if (this.isSuccess())
				this.c = DLMCategory.categories.get(DLMCategory.Type.CANCEL);
			else
				this.c = DLMCategory.categories.get(DLMCategory.Type.CANCEL_ERROR);
		} else {
			if (this.isSuccess())
				this.c = DLMCategory.categories.get(DLMCategory.Type.UNLOCK);
			else
				this.c = DLMCategory.categories.get(DLMCategory.Type.UNLOCK_ERROR);
		}
        }

	public Boolean isCancel()
	{
		return ((this.getFlags() & 0x2) == 0x2);
	}
}
