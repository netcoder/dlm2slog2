// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.linux;

import java.util.*;
import tracecmd.*;

public class DLMLineID
{
	private DLMResource res;
	private Integer nodeid;

	public DLMLineID(DLMResource res, Integer nodeid)
	{
		this.res = res;
		this.nodeid = nodeid;
	}

	@Override
	public boolean equals(Object o)
	{
		DLMLineID lineid;

		if (o instanceof DLMLineID) {
			lineid = (DLMLineID)o;

			return this.res.equals(lineid.res) &&
			       this.nodeid.equals(lineid.nodeid);
		}

		return false;
	}

	public DLMResource getResource()
	{
		return this.res;
	}

	public Integer getNodeId()
	{
		return this.nodeid;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(this.res, this.nodeid);
	}
}
