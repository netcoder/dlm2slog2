// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.slog2.output;

import logformat.slog2.output.*;

public class DLMOutputLog extends OutputLog
{
    public DLMOutputLog(String filename)
    {
	    super(filename);
    }

    @Override
    protected void finalize() throws Throwable
    {
	    /* bugfix */
    }
}
