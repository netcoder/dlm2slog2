/*
 * This file is copied from slog2sdk and a little bit changed by Alexander
 * Aring <aahringo@redhat.com>. The slog2sdk contains the following COPYRIGHT
 * file.
 *
 *                          COPYRIGHT
 *
 * The following is a notice of limited availability of the code, and disclaimer
 * which must be included in the prologue of the code and in all source listings
 * of the code.
 *
 * Copyright Notice 2001 University of Chicago
 *
 * Permission is hereby granted to use, reproduce, prepare derivative works, and
 * to redistribute to others.  This software was authored by:
 *
 * Argonne National Laboratory Group
 * A. Chan:  (630) 252-6044; FAX: (630) 252-5986; e-mail: chan@mcs.anl.gov
 * W. Gropp: (630) 252-4318; FAX: (630) 252-5986; e-mail: gropp@mcs.anl.gov
 * E. Lusk:  (630) 252-7852; FAX: (630) 252-5986; e-mail: lusk@mcs.anl.gov
 *
 * Mathematics and Computer Science Division
 * Argonne National Laboratory, Argonne IL 60439
 *
 *
 *                     GOVERNMENT LICENSE
 *
 * Portions of this material resulted from work developed under a U.S.
 * Government Contract and are subject to the following license: the Government
 * is granted for itself and others acting on its behalf a paid-up, nonexclusive,
 * irrevocable worldwide license in this computer software to reproduce, prepare
 * derivative works, and perform publicly and display publicly.
 *
 *                         DISCLAIMER
 *
 * This computer code material was prepared, in part, as an account of work
 * sponsored by an agency of the United States Government.  Neither the United
 * States, nor the University of Chicago, nor any of their employees, makes any
 * warranty express or implied, or assumes any legal liability or responsibility
 * for the accuracy, completeness, or usefulness of any information, apparatus,
 * product, or process disclosed, or represents that its use would not infringe
 * privately owned rights.
 *
 *  (C) 2001 by Argonne National Laboratory
 *
 *  @author  Anthony Chan
 */

package dlm.slog2.output;

import java.math.*;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.*;
import java.nio.file.*;

import base.drawable.*;
import logformat.slog2.*;
import logformat.slog2.output.*;

import dlm.linux.*;
import tracecmd.*;

public class DLMToSlog2
{
	public static void do_slog2(dlm.linux.InputLog dobj_ins,
				    String output_filename) throws IOException
	{
		short    num_children_per_node = 0;
		int      leaf_bytesize         = 0;
		String   trace_filespec = null, slog_filename;
		boolean  enable_endtime_check = true;
		boolean  continue_when_violation;

		dlm.slog2.output.DLMOutputLog	    slog_outs;
		Kind                                next_kind;
		Topology                            topo;
		CategoryMap                         objdefs;   // Drawable def'n
		Map<Topology, Category>             shadefs;   // Shadow   def'n
		Category                            objdef;
		LineIDMapList                       lineIDmaps;
		LineIDMap                           lineIDmap;
		Primitive                           prime_obj;
		base.drawable.Composite             cmplx_obj;
		long                                Nobjs;

		TreeTrunk                           treetrunk;
		double                              prev_dobj_endtime;
		double                              curr_dobj_endtime;
		long                                offended_Nobjs;
		Drawable                            offended_dobj;

		//  Initialize prev_dobj_endtime to avoid complaint by compiler
		prev_dobj_endtime = Double.NEGATIVE_INFINITY;
		offended_Nobjs    = Integer.MIN_VALUE;
		offended_dobj     = null;

		slog_filename = output_filename;
		if (slog_filename == null)
			slog_filename = TraceName.getDefaultSLOG2Name("output.slog2");

		Path fileToDeletePath = Paths.get(slog_filename);
		Files.deleteIfExists(fileToDeletePath);

		objdefs       = new CategoryMap();
		shadefs       = new HashMap<Topology, Category>();
		lineIDmaps    = new LineIDMapList();
		Nobjs         = 0;

		/* */    Date time1 = new Date();
		slog_outs  = new dlm.slog2.output.DLMOutputLog(slog_filename);

		//  Set Tree properties, !optional!
		//  TreeNode's minimum size, without any drawable/shadow, is 38 bytes.
		//  Drawable;s minimum size is 32 bytes, whether it is state/arrow.
		//  Arrow( with 2 integer infovalues ) is 40 bytes long.
		//  So, for 1 state primitive leaf, the size is 38 + 40 = 78 .
		if ( leaf_bytesize > 0 )
			slog_outs.setTreeLeafByteSize( leaf_bytesize );
		if ( num_children_per_node > 0 )
			slog_outs.setNumChildrenPerNode( num_children_per_node );

		treetrunk = new TreeTrunk( slog_outs, shadefs );
		/* */    Date time2 = new Date();
		while ( ( next_kind = dobj_ins.peekNextKind() ) != Kind.EOF ) {
			if ( next_kind == Kind.TOPOLOGY ) {
				topo = dobj_ins.getNextTopology();
				objdef = Category.getShadowCategory( topo );
				objdefs.put( objdef.getIndex() , objdef );
				shadefs.put( topo, objdef );
			}
			else if ( next_kind == Kind.YCOORDMAP ) {
				lineIDmap = new LineIDMap( dobj_ins.getNextYCoordMap() );
				lineIDmaps.add( lineIDmap );
			}
			else if ( next_kind == Kind.CATEGORY ) {
				objdef = dobj_ins.getNextCategory();
				objdefs.put( objdef.getIndex() , objdef );
			}
			else if ( next_kind == Kind.PRIMITIVE ) {
				prime_obj = dobj_ins.getNextPrimitive();
				prime_obj.resolveCategory( objdefs );
				// postponed, wait till on-demand decoding of InfoBuffer
				// prime_obj.decodeInfoBuffer();
				Nobjs++;
				// System.out.println( Nobjs + " : " + prime_obj );
				if ( enable_endtime_check ) {
					if ( ! prime_obj.isTimeOrdered() ) {
						System.out.println( "**** Primitive Time Error ****" );
					}
					curr_dobj_endtime = prime_obj.getLatestTime();
					if ( prev_dobj_endtime > curr_dobj_endtime ) {
						System.err.println( "**** Violation of "
								+ "Increasing Endtime Order ****\n"
								+ "\t Offended Drawable -> "
								+ offended_Nobjs + " : "
								+ offended_dobj + "\n"
								+ "\t Offending Primitive -> "
								+ Nobjs + " : " + prime_obj + "\n"
								+ "   previous drawable endtime ( "
								+ prev_dobj_endtime + " ) "
								+ " > current drawable endtiime ( "
								+ curr_dobj_endtime + " ) " );
					}
					offended_Nobjs    = Nobjs;
					offended_dobj     = prime_obj;
					prev_dobj_endtime = curr_dobj_endtime;
				}
				treetrunk.addDrawable( prime_obj );
			}
			else if ( next_kind == Kind.COMPOSITE ) {
				cmplx_obj = dobj_ins.getNextComposite();
				cmplx_obj.resolveCategory( objdefs );
				// postponed, wait till on-demand decoding of InfoBuffer
				// cmplx_obj.decodeInfoBuffer();
				Nobjs++;
				// System.out.println( Nobjs + " : " + cmplx_obj );
				if ( enable_endtime_check ) {
					if ( ! cmplx_obj.isTimeOrdered() ) {
						System.out.println( "**** Composite Time Error ****" );
					}
					curr_dobj_endtime = cmplx_obj.getLatestTime();
					if ( prev_dobj_endtime > curr_dobj_endtime ) {
						System.err.println( "***** Violation of "
								+ "Increasing Endtime Order! *****\n"
								+ "\t Offended Drawable -> "
								+ offended_Nobjs + " : "
								+ offended_dobj + "\n"
								+ "\t Offending Composite -> "
								+ Nobjs + " : " + cmplx_obj + "\n"
								+ "   previous drawable endtime ( "
								+ prev_dobj_endtime + " ) "
								+ " > current drawable endtiime ( "
								+ curr_dobj_endtime + " ) " );
					}
					offended_Nobjs    = Nobjs;
					offended_dobj     = cmplx_obj;
					prev_dobj_endtime = curr_dobj_endtime;
				}
				treetrunk.addDrawable( cmplx_obj );
			}
			else {
				System.err.println( "TraceToSlog2: Unrecognized return "
						+ "from peekNextKind() = " + next_kind );
			}
		}   // Endof while ( dobj_ins.peekNextKind() )

		// Check if flushToFile is successful,
		// i.e. if treetrunk contains drawables.
		if ( treetrunk.flushToFile() ) {
			objdefs.removeUnusedCategories();
			slog_outs.writeCategoryMap( objdefs );

			lineIDmaps.add( treetrunk.getIdentityLineIDMap() );
			slog_outs.writeLineIDMapList( lineIDmaps );

			slog_outs.close();
			dobj_ins.close();
		} else {
			slog_outs.close();
			dobj_ins.close();
			System.err.println( "Error: No drawable is found in the trace "
					+ trace_filespec );
			File slog_file  = new File( slog_filename );
			if ( slog_file.isFile() && slog_file.exists() ) {
				System.err.println( "       Removing the remnants of the file "
						+ slog_filename + " ....." );
				slog_file.delete();
			}
		}

		Date time3 = new Date();
		System.out.println( "\n" );
		System.out.println( "Number of Drawables = " + Nobjs );

		System.out.println( "timeElapsed between 1 & 2 = "
				+ ( time2.getTime() - time1.getTime() ) + " msec" );
		System.out.println( "timeElapsed between 2 & 3 = "
				+ ( time3.getTime() - time2.getTime() ) + " msec" );
	}
}
