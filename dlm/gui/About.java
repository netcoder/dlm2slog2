// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;

public class About extends JDialog
{
	public About(JFrame parent)
	{
		super(parent, "About", true);

		Box b = Box.createVerticalBox();
		b.add(Box.createGlue());
		b.add(new JLabel("Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>"));
		b.add(new JLabel("https://gitlab.com/netcoder/dlm2slog2"));
		b.add(Box.createGlue());
		getContentPane().add(b, "Center");

		JPanel p2 = new JPanel();
		JButton ok = new JButton("Ok");
		p2.add(ok);
		getContentPane().add(p2, "South");

		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt)
			{
				setVisible(false);
			}
		});

		this.pack();
	}
}
