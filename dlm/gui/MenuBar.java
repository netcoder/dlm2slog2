// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;

public class MenuBar extends JMenuBar
{
	public MenuBar()
	{
		JMenu menu = new JMenu("Menu");
		JMenuItem a1 = new JMenuItem("Exit");
		a1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		menu.add(a1);

		JMenu menu2 = new JMenu("About");
		JMenuItem b1 = new JMenuItem("Info");
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				JDialog d = new About(MainWindow.mw);
				d.setLocationRelativeTo(null);
				d.setVisible(true);
			}
		});
		menu2.add(b1);

		this.add(menu);
		this.add(menu2);
	}
}
