// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import javax.swing.*;
import java.util.*;
import java.awt.*;

public class ListView extends JPanel
{
	public ListView(String label)
	{
		super.setLayout(new BorderLayout());

		JList<String> listBox = new JList<>();
		listBox.setBorder(BorderFactory.createLineBorder(Color.black));
		listBox.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		listBox.setVisibleRowCount(-1);
		listBox.setSelectedIndex(-1);

		this.add(listBox, BorderLayout.CENTER);
	}
}
