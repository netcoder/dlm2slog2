// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import dlm.linux.*;
import dlm.slog2.output.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.*;

public class SaveSlog2Panel extends JPanel
{
	private TraceFilePanel tracepanel;
	private dlm.linux.InputLog dobj_ins;
	private DLMTableModel available_model;
	private DLMTableModel filtered_model;
	private JTextField path;

	public SaveSlog2Panel(String textinput, dlm.linux.InputLog dobj_ins,
			      TraceFilePanel tracepanel,
			      DLMTableModel available_model,
			      DLMTableModel filtered_model)
	{
		GridBagConstraints c;

		this.tracepanel = tracepanel;
		this.dobj_ins = dobj_ins;
		this.available_model = available_model;
		this.filtered_model = filtered_model;

		this.setLayout(new GridBagLayout());

		this.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		this.setMaximumSize(new Dimension(16777215, 42));

		c = new GridBagConstraints();
		c.insets = new Insets(0,0,0,5);
		c.gridx = 0;
		c.gridy = 0;
		JButton jbtn1 = new JButton("Select Slog2 Filepath");
		jbtn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

					public String getDescription() {
						return "slog2 (*.slog2)";
					}

					public boolean accept(File f) {
						if (f.isDirectory()) {
							return true;
						} else {
							String filename = f.getName().toLowerCase();
							return filename.endsWith(".slog2");
						}
					}
				});
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				int result = fileChooser.showSaveDialog(SaveSlog2Panel.this);
				if (result == JFileChooser.APPROVE_OPTION) {
					File fileToBeSaved = fileChooser.getSelectedFile();

					if(!fileChooser.getSelectedFile().getAbsolutePath().endsWith(".slog2")){
						fileToBeSaved = new File(fileChooser.getSelectedFile() + ".slog2");
					}

					System.out.println("Selected file: " + fileToBeSaved.getAbsolutePath());
				}
			}
		});
		this.add(jbtn1, c);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,5);
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		path = new JTextField(textinput);
		this.add(path, c);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,0);
		c.weightx = 0;
		c.gridx = 2;
		c.gridy = 0;
		JButton savebtn = new JButton("Generate and Save");
		savebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Set<DLMResource> filter = new HashSet<DLMResource>();
				DLMResource[] resarray = new DLMResource[filtered_model.getRowCount()];

				resarray = filtered_model.toArray(resarray);
				for (int i = 0; i < resarray.length; i++) {
					filter.add(resarray[i]);
				}

				try {
					String filepath = SaveSlog2Panel.this.path.getText();

					if (!filepath.endsWith(".slog2"))
						throw new Exception("wrong filename must end with .slog2");

					SaveSlog2Panel.this.dobj_ins.parseLog(tracepanel.getTraceFiles().keySet(), filter);
					DLMToSlog2.do_slog2(SaveSlog2Panel.this.dobj_ins, filepath);
				} catch (Exception ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(MainWindow.mw, ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}

				JOptionPane.showMessageDialog(MainWindow.mw, "DONE");
			}
		});
		this.add(savebtn, c);

	}
}
