// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.util.stream.Collectors.*;
import java.awt.event.*;
import javax.swing.*;
import dlm.linux.*;
import java.util.*;
import java.awt.*;

public class MainWindow extends JFrame
{
	public static MainWindow mw;

	private DLMTableModel available_model;
	private DLMTableModel filtered_model;
	private TraceFilePanel tracepanel;
	private JProgressBar progressBar;
	private InputLog dobj_ins;
	private JPanel content;

	/* default config */
	private static String slog2_default = System.getProperty("user.home");
	private static Map<Integer, String> traces_default;
	static {
		traces_default = new HashMap<Integer, String>();

		traces_default.put(1, System.getProperty("user.home"));
		traces_default.put(2, System.getProperty("user.home"));
		traces_default.put(3, System.getProperty("user.home"));
	}


	public MainWindow(String name)
	{
		super(name);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLayout(new BorderLayout(32, 32));
		super.setSize(1280, 800);
		super.setJMenuBar(new MenuBar());

		this.tracepanel = new TraceFilePanel();
		this.available_model = new DLMTableModel();
		this.filtered_model = new DLMTableModel();
		this.progressBar = new JProgressBar(SwingConstants.HORIZONTAL, 0, 100);

		this.dobj_ins = new dlm.linux.InputLog(progressBar);

		GridBagConstraints c;
		content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		add(content, BorderLayout.CENTER);

		AddNodePanel nodepanel = new AddNodePanel(tracepanel);
		content.add(nodepanel);

		for (Map.Entry<Integer, String> entry:traces_default.entrySet()) {
			tracepanel.add_entry(entry.getKey(), entry.getValue());
		}
		content.add(tracepanel);

		AnalyzePanel analyzepanel = new AnalyzePanel(this.dobj_ins, this.tracepanel,
							     this.available_model, this.filtered_model);
		content.add(analyzepanel);

		content.add(new ResourcesPanel(this.available_model, this.filtered_model));

		SaveSlog2Panel ss2p = new SaveSlog2Panel(slog2_default, this.dobj_ins, this.tracepanel,
							 this.available_model, this.filtered_model);
		content.add(ss2p);

		JPanel p2 = new JPanel();
		p2.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		p2.setLayout(new BorderLayout());
		p2.setMaximumSize(new Dimension(16777215, 42));

		progressBar.setStringPainted(true);
		p2.add(progressBar);
		content.add(p2);

		this.centreWindow();
	}

	private void centreWindow()
	{
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

		int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
		this.setLocation(x, y);
	}


	public static final void main(String[] args)
	{
		for (int i=0; i < args.length; i++) {
			if (args[i].equals("--help")) {
				System.exit(0);
			}
		}

		try {
			for (int i=0; i < args.length; i++) {
				if (args[i].equals("--traces")) {
					Map<Integer, String> map = new HashMap<Integer, String>();
					String[] pairs = args[i+1].split(":");
					for (String s:pairs) {
						String[] tokens = s.split("=");

						for (int j=0; j < tokens.length - 1;) {
							map.put(Integer.parseInt(tokens[j++]), tokens[j++]);
						}
					}

					traces_default = map;
				}
				if (args[i].equals("--slog2")) {
					slog2_default = args[i+1];
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to parse arguments");
			System.exit(1);
		}

		mw = new MainWindow("DLMToSlog2");
		mw.setVisible(true);
	}
}
