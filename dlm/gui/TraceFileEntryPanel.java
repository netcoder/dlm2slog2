// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.awt.*;

public class TraceFileEntryPanel extends JPanel
{
	private TraceFilePanel parent;
	private JSpinner spinner;
	private JTextField path;

	public TraceFileEntryPanel(TraceFilePanel parent, int nodeid, String textinput)
	{
		this.parent = parent;
		GridBagConstraints c;

		setLayout(new GridBagLayout());

		c = new GridBagConstraints();
		c.insets = new Insets(0,0,0,5);
		c.gridx = 0;
		c.gridy = 0;
		JButton delbtn = new JButton("-");
		delbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (parent.getEntriesCount() == 1)
					return;

				parent.del_entry(TraceFileEntryPanel.this);
			}
		});
		add(delbtn, c);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,5);
		c.gridx = 1;
		c.gridy = 0;
		add(new JLabel("Nodeid:"), c);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,5);
		c.gridx = 2;
		c.gridy = 0;
		SpinnerNumberModel model = new SpinnerNumberModel(nodeid, 1, 5, 1);
		spinner = new JSpinner(model);
		spinner.setValue(nodeid);
		add(spinner, c);

		path = new JTextField(textinput);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,5);
		c.gridx = 3;
		c.gridy = 0;
		JButton tfile = new JButton("Select Linux Trace Filepath");
		tfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
				int result = fileChooser.showOpenDialog(TraceFileEntryPanel.this);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					TraceFileEntryPanel.this.path.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		add(tfile, c);

		c = new GridBagConstraints();
		c.insets = new Insets(0,5,0,0);
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 4;
		c.gridy = 0;
		add(path, c);
	}

	public int getNodeId()
	{
		return (Integer)this.spinner.getValue();
	}

	public String getPath()
	{
		return this.path.getText();
	}
}
