// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import javax.swing.*;
import dlm.linux.*;
import java.util.*;
import java.math.*;
import javax.swing.table.*;

public class DLMTableModel extends AbstractTableModel
{
	private final String headers[] = { "#Events", "Lockspace ID", "Resource Name" };
	private final List<DLMResource> resources = new ArrayList<DLMResource>();
	private JTable table;

	private final Class[] columnClass = new Class[] {
		Integer.class, Integer.class, String.class
	};

	public void setTable(JTable table)
	{
		this.table = table;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		return false;
	}

	@Override
	public String getColumnName(int column)
	{
		return this.headers[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return columnClass[columnIndex];
	}

	@Override
	public int getColumnCount()
	{
		return headers.length;
	}

	@Override
	public int getRowCount()
	{
		return this.resources.size();
	}

	@Override
	public Object getValueAt(int row, int column)
	{
            DLMResource res = resources.get(row);

            switch (column) {
                case 0:
                    return res.getNumEvents();
                case 1:
                    return String.format("0x08%x", res.getLsId());
                case 2:
                    return res.getResName();
		default:
		    return null;
            }
	}

	public void addEntry(DLMResource res)
	{
		this.resources.add(res);
		this.fireTableRowsInserted(this.resources.size() - 1,
					   this.resources.size() - 1);
	}

	public void delEntry(DLMResource res)
	{
		int index = this.resources.indexOf(res);
		this.resources.remove(res);
		this.fireTableRowsDeleted(index - 1, index -1);
	}

	public DLMResource getEntry(int index)
	{
		return this.resources.get(index);
	}

	public DLMResource[] toArray(DLMResource[] arr)
	{
		return this.resources.toArray(arr);
	}

	public void sort()
	{
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(sorter);
		ArrayList<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
		sortKeys.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
		sorter.setSortKeys(sortKeys);
		sorter.sort();
	}

	public void clear()
	{
		int size = this.resources.size();

		this.resources.clear();
		if (size != 0)
			this.fireTableRowsDeleted(0, size - 1);
	}
}
