// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;

public class AddNodePanel extends JPanel
{
	private TraceFilePanel parent;

	public AddNodePanel(TraceFilePanel parent)
	{
		this.parent = parent;

		setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
		setLayout(new BorderLayout());
		setMaximumSize(new Dimension(16777215, 42));
		JButton btn = new JButton("Add Node");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				if (AddNodePanel.this.parent.getEntriesCount() == 5)
					return;

				parent.add_entry(null, null);
			}
		});
		add(btn);
	}
}
