// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import dlm.linux.*;
import java.awt.*;

public class AnalyzePanel extends JPanel
{
	private TraceFilePanel tracepanel;
	private dlm.linux.InputLog dobj_ins;
	private DLMTableModel available_model;
	private DLMTableModel filtered_model;

	public AnalyzePanel(dlm.linux.InputLog dobj_ins,
			    TraceFilePanel tracepanel,
			    DLMTableModel available_model,
			    DLMTableModel filtered_model)
	{
		this.dobj_ins = dobj_ins;
		this.tracepanel = tracepanel;
		this.available_model = available_model;
		this.filtered_model = filtered_model;

		setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		setLayout(new BorderLayout());
		setMaximumSize(new Dimension(16777215, 42));
		JButton analyzebutton = new JButton("Analyze Resources");
		analyzebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Map<DLMResource, DLMResource> map;

				AnalyzePanel.this.available_model.clear();
				AnalyzePanel.this.filtered_model.clear();

				try {
					map = AnalyzePanel.this.dobj_ins.getResourceNames(AnalyzePanel.this.tracepanel.getTraceFiles());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(MainWindow.mw, ex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
					return;
				}

				for (DLMResource r:map.keySet()) {
					AnalyzePanel.this.available_model.addEntry(r);
				}

				AnalyzePanel.this.available_model.sort();
			}
		});
		add(analyzebutton);
	}

}
