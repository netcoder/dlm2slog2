// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;

public class TraceFilePanel extends JPanel
{
	private ArrayList<TraceFileEntryPanel> entries;

	public TraceFilePanel()
	{
		GridBagConstraints c;

		this.entries = new ArrayList<TraceFileEntryPanel>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		setMaximumSize(new Dimension(16777215, 42));

		JPanel tracefp = new JPanel();
		tracefp.setLayout(new GridBagLayout());
	}

	public int getEntriesCount()
	{
		return this.entries.size();
	}

	public void add_entry(Integer nodeid, String textinput)
	{
		TraceFileEntryPanel entry = new TraceFileEntryPanel(this, (nodeid != null)?nodeid:this.getEntriesCount() + 1,
								    (textinput != null)?textinput:System.getProperty("user.home"));
		this.entries.add(entry);
		add(entry);
		revalidate();
		repaint();
	}

	public void del_entry(TraceFileEntryPanel entry)
	{
		remove(entry);
		revalidate();
		repaint();
		this.entries.remove(entry);
	}

	public HashMap<Integer, String> getTraceFiles()
	{
		HashMap<Integer, String> ret = new HashMap<Integer, String>();

		for (TraceFileEntryPanel t:this.entries) {
			ret.put(t.getNodeId(), t.getPath());
		}

		return ret;
	}
}
