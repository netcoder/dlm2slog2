// SPDX-License-Identifier: BSD-3-Clause
/*
 * Copyright (C) 2022 Red Hat Inc, Alexander Aring <aahringo@redhat.com>
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package dlm.gui;

import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.*;
import dlm.linux.*;
import java.util.*;
import java.awt.*;

public class ResourcesPanel extends JPanel
{
	private JTable available_table;
	private DLMTableModel available_model;
	private JTable filtered_table;
	private DLMTableModel filtered_model;

	public ResourcesPanel(DLMTableModel available_model,
			      DLMTableModel filtered_model)
	{
		GridBagConstraints c;
		JButton ltor;
		JButton rtol;

		this.available_model = available_model;
		this.filtered_model = filtered_model;

		this.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		this.setLayout(new GridBagLayout());
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		this.add(new JLabel("Available Resources", JLabel.CENTER), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 1;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 1;

		this.available_table = new JTable(this.available_model);
		this.available_model.setTable(available_table);
		available_table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		JScrollPane scrollPane_avail = new JScrollPane(available_table);

		this.add(scrollPane_avail, c);

		this.filtered_table = new JTable(this.filtered_model);
		JScrollPane scrollPane_filter = new JScrollPane(filtered_table);
		filtered_table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		this.filtered_model.setTable(filtered_table);

		JPanel bts = new JPanel();
		bts.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		bts.setLayout(new BorderLayout(5, 5));

		ltor = new JButton(">>");
		ltor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				java.util.List<DLMResource> to_del = new ArrayList<DLMResource>();
				int[] sel = ResourcesPanel.this.available_table.getSelectedRows();

				for (int i=0; i < sel.length; i++) {
					sel[i] = ResourcesPanel.this.available_table.convertRowIndexToModel(sel[i]);
					to_del.add(ResourcesPanel.this.available_model.getEntry(sel[i]));
				}

				for (DLMResource r:to_del) {
					ResourcesPanel.this.filtered_model.addEntry(r);
					ResourcesPanel.this.available_model.delEntry(r);
				}

				ResourcesPanel.this.available_table.clearSelection();
				ResourcesPanel.this.filtered_model.sort();
				ResourcesPanel.this.available_model.sort();
			}
		});

		bts.add(ltor, BorderLayout.NORTH);

		rtol = new JButton("<<");
		rtol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				java.util.List<DLMResource> to_del = new ArrayList<DLMResource>();
				int[] sel = ResourcesPanel.this.filtered_table.getSelectedRows();

				for (int i=0; i < sel.length; i++) {
					sel[i] = ResourcesPanel.this.filtered_table.convertRowIndexToModel(sel[i]);
					to_del.add(ResourcesPanel.this.filtered_model.getEntry(sel[i]));
				}

				for (DLMResource r:to_del) {
					ResourcesPanel.this.available_model.addEntry(r);
					ResourcesPanel.this.filtered_model.delEntry(r);
				}

				ResourcesPanel.this.filtered_table.clearSelection();
				ResourcesPanel.this.filtered_model.sort();
				ResourcesPanel.this.available_model.sort();
			}
		});
		bts.add(rtol, BorderLayout.SOUTH);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		this.add(bts, c);

		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 0;
		this.add(new JLabel("Filtered Resources", JLabel.CENTER), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weighty = 1;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 1;
		this.add(scrollPane_filter, c);
	}
}
